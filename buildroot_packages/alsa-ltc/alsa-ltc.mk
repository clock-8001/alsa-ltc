################################################################################
#
# alsa-ltc, a audio to OSC SMTPE LTC converter
#
################################################################################

ALSA_LTC_VERSION = 86b66fa8e1f0780b3ff99ca10ef883a22f4ec309
ALSA_LTC_SITE_METHOD = git
ALSA_LTC_SITE = https://gitlab.com/clock-8001/alsa-ltc
ALSA_LTC_DEPENDENCIES = host-pkgconf alsa-lib libltc

define ALSA_LTC_BUILD_CMDS
	cd $(@D) && $(MAKE) $(TARGET_CONFIGURE_OPTS)
endef

define ALSA_LTC_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/alsa-ltc $(TARGET_DIR)/root/alsa-ltc

	# init scripts
	$(INSTALL) -D -m 0755 $(ALSA_LTC_PKGDIR)/S03copy_alsa-ltc_files $(TARGET_DIR)/etc/init.d/S03copy_alsa-ltc_files
	$(INSTALL) -D -m 0755 $(ALSA_LTC_PKGDIR)/S99alsa-ltc $(TARGET_DIR)/etc/init.d/S99alsa-ltc

	$(INSTALL) -D -m 0755 $(ALSA_LTC_PKGDIR)/alsa-ltc_pokemon.sh $(TARGET_DIR)/root/alsa-ltc_pokemon.sh
	echo "/root/alsa-ltc $(BR2_PACKAGE_ALSA_LTC_CONFIG)" > $(@D)/alsa-ltc_cmd.sh
	$(INSTALL) -D -m 0755 $(@D)/alsa-ltc_cmd.sh $(TARGET_DIR)/root/alsa-ltc_cmd.sh
endef

$(eval $(generic-package))
