all: alsa-ltc

alsa-ltc: alsa-ltc.c tinyosc/tinyosc.c tinyosc/tinyosc.h
	$(CC) -Wall -o alsa-ltc alsa-ltc.c tinyosc/tinyosc.c -lasound -lm -lltc
